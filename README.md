

# Projeto Integrador 2

## Configuração

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.


## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

``
nav:
  - Home: 'index.md'
  - 'Semestres Anteriores': 'anteriores.md'

``
3. testar localmente
# Visualizando 

Para visualizar os relatórios, acessar :

[https://fga-pi2.gitlab.io/projeto-integrador-2](https://fga-pi2.gitlab.io/projeto-integrador-2)
