## Plano de ensino

|**Disciplina:** Projeto Integrador 2|**Código:** FGA0250|
|:----|:----|
|**Curso:** Engenharias|Semestre/Ano: 01/2024|
|**Professores:**|
|Alex Reis (Eng. de Energia)
Carla Silva Rocha Aguiar (Eng. de Software)
Rhander Viana (Eng. Automotiva)
Suelia de Siqueira Rodrigues Fleury Rosa (Eng. Eletrônica)
Rafael Castilho Faria Mendes (Eng. Aeroespacial)|Carga horária: 90 h|
|**Horário das aulas:**
Quarta-feira: 16h00min as 17h50min. Local: S1
Sexta-feira: 14h00min as 17h50min. Local: FGA Anfiteatro| |
|**Salas** - [Microsoft Teams](https://teams.microsoft.com/l/team/19%3a111fb079fe9e424f87bdf0c1a9b17124%40thread.tacv2/conversations?groupId=5b1c5ea7-8fc6-43a9-8a10-998665a886e9&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059)| |


## Ementa

Consolidar, em projetos práticos, os conhecimentos adquiridos nas
disciplinas dos cursos de engenharia da Faculdade Gama (FGA):
automotiva, eletrônica, energia, aeroespacial e software, a fim de
solucionar um problema da vida real definido pelos estudantes e
professores orientadores da disciplina.

## Objetivos da disciplina

Ao final da disciplina o estudante será capaz de:

1.  Identificar problemas da vida real, cujas soluções envolvam
    conhecimentos multidisciplinares de mais de uma engenharia;

2.  Entender a terminologia e fundamentos básicos de um problema,
    avaliando a viabilidade de sua solução, por meio do estabelecimento
    de um projeto no contexto dos recursos disponíveis na FGA;

3.  Estabelecer, especificar e executar um projeto de engenharia,
    visando a solução de um problema;

4.  Apresentar os produtos resultantes de um projeto;

5.  Desenvolver a habilidade de geração de novas soluções para problemas
    de engenharia, por meio da análise, síntese e otimização de
    sistemas;

6.  Promover a interdisciplinaridade;

7.  Desenvolver a capacidade de comunicação técnica escrita e oral;

8.  Desenvolver a capacidade de pensamento crítico independente,
    investigação racional e autoaprendizagem;

9.  Desenvolver a capacidade de trabalho em equipe;

10. Promover a compreensão das responsabilidades sociais, culturais e
    ambientais do engenheiro e a necessidade do desenvolvimento
    sustentável.

## Conteúdo Programático

O conteúdo programático desta disciplina é dependente do tipo e
abrangência do problema definido pelos estudantes e professores
orientadores. Todavia, de forma comum a todos os projetos, os seguintes
assuntos serão desenvolvidos pelos estudantes: Práticas de Gerenciamento
de Projeto; Desenvolvimento de protótipos de produtos.

Nesse contexto, o desenvolvimento do projeto seguirá o ciclo de vida
composto pelas fases descritas a seguir.

**Fase 1: Problematização**

> Objetivo geral:

1.  Definir um problema prático que possa ser resolvido utilizando
    conhecimentos multidisciplinares. Utilizam-se, como referência, as
    áreas de conhecimento dos cursos de engenharia da FGA: automotiva,
    eletrônica, energia, aeroespacial e software.

> Objetivos específicos

-   Refinar o entendimento do problema a ser resolvido com conhecimentos
    de engenharias, seu escopo e abrangência;

-   Refinar o problema a ser resolvido, a fim de identificar seus
    principais requisitos (ou objetivos) funcionais e não funcionais;

-   Analisar a viabilidade técnica e financeira a partir de alguns
    requisitos básicos, como tempo (prazo de 1 semestre letivo), preço
    (o projeto será financiado pelo grupo), desafios técnicos, etc.

**Fase 2: Concepção e detalhamento da solução**

> Objetivo geral:

1.  Conceber e detalhar os itens da arquitetura básica da solução a ser
    utilizada no projeto, envolvendo as diversas áreas de conhecimento;

2.  Definir aspectos relacionados ao gerenciamento das atividades do
    projeto.

> Objetivos específicos

-   Descrever os requisitos a serem satisfeitos pelo projeto, assim como
    seus objetivos e regras de negócios, considerando os recursos da FGA
    (máquinas, equipamentos, laboratórios e ferramentas de engenharias);

-   Refinar a arquitetura da solução, considerando a identificação e
    descrição de todos os conteúdos teóricos a serem utilizados na
    solução;

-   Gerenciamento do projeto: a partir da metodologia definida no PMBOK
    (*Project Management Body of Knowledge*), os seguintes documentos
    deverão ser entregues ao final desta fase:

    -   Termo de Abertura do Projeto (TAP);

    -   Estrutura Analítica de Projeto (EAP) ou *Work Breakdown
        Structure* (WBS);

    -   Tempo: Definição de atividades; Sequenciamento de atividades;
        Cronograma de atividades;

    -   Custos: Estimativa de custos e orçamentos para a realização do
        projeto;

    -   Recursos humanos: alocação dos recursos humanos nos subsistemas
        que compõe o projeto;

    -   Riscos: Levantamento de riscos para a execução do projeto e
        avaliação do impacto; plano de contingências.

**Fase 3: Projeto e construção de subsistemas da solução proposta**

> Objetivo geral:

1.  Projeto de solução: modelagem e cálculos matemáticos, simulação,
    testes computacionais, etc.

2.  Construir os componentes e/ou subsistemas da solução prevista pelo
    projeto prático de engenharias

> Objetivos específicos

-   Realizar o projeto dos componentes/subsistemas que compõe a solução,
    baseado em critérios técnicos de engenharia;

-   Construir componentes /subsistemas;

-   Testar componentes/subsistemas da solução.

-   Avaliar e homologar resultados

**Fase 4: Integração de subsistemas e finalização do produto**

> Objetivo geral:

-   Executar o projeto de integração dos componentes/subsistemas,
    conforme a arquitetura da solução proposta pela equipe;

-   Implantar o produto final previsto como resultado do projeto de
    engenharia.

> Objetivos específicos

-   Integrar componentes da solução;

-   Testar o produto final e comprovar o funcionamento da solução;

-   Avaliar e homologar o produto final do projeto.

## Metodologia de ensino e recursos necessários

A seguir, estão listados alguns aspectos acerca da metodologia de ensino
e dos recursos necessários para execução do projeto:

1.  Os projetos serão desenvolvidos por **grupo de alunos**, sendo
    constituído por estudantes de todos os cursos de engenharia da FGA e
    respeitando a proporção dos matriculados. A quantidade final de
    estudantes por grupo poderá ser ajustada pelos professores, para
    proporcionar a maior isonomia possível.

2.  As aulas são dedicadas à compreensão, aquisição e aplicação de
    conhecimentos, viabilizando o desenvolvimento do projeto de acordo
    com o ciclo de vida do explicitado anteriormente. Nesse contexto,
    serão utilizadas as seguintes estratégias de ensino:

    -   Aprendizagem baseada em Projetos (*Project-Based Learning* -
        PBL): investigações e estudos são realizadas de forma autônoma
        pelos grupos, com enfoque no desenvolvimento da solução. Os
        professores-tutores realizam a gestão da disciplina,
        direcionando as atividades do projeto e auxiliando na construção
        da solução;

3.  **A obtenção de recursos financeiros para a execução dos projetos é
    responsabilidade do próprio grupo. A utilização de recursos
    existentes na FGA (máquinas, equipamentos, ferramentas, etc.) deverá
    ser acordada com o responsável do laboratório (professor ou
    coordenador de laboratório) onde o equipamento se localiza. Neste
    último caso, deverá ser respeitada: as atividades de ensino já
    programadas para o local; o horário de funcionamento; e a presença
    do técnico de laboratório no ambiente de trabalho.**

4.  Para as atividades desenvolvidas em laboratórios da FGA, os
    estudantes deverão, impreterivelmente, seguir as regras de
    utilização do espaço. Para minimizar o risco de acidentes,
    **solicita-se que os estudantes utilizem calçados fechados, calças
    compridas e camisas de mangas, além dos Equipamento de Proteção
    Coletivos e Individuais específicos do ambiente.**

## Organização dos grupos 

A organização interna dos grupos para o desenvolvimento das atividades
deverá seguir o organograma apresentado na Figura 01, em que estão
definidos os "papeis" a ser desempenhado pelos integrantes do grupo. O
detalhamento das atividades referentes a cada papel está descrito no
apêndice deste documento.



Observações:

-   Quantitativo máximo de integrantes, por categoria:

    -   Coordenador geral: apenas 01 integrante do grupo;

    -   Diretor de Qualidade: apenas 01 integrante do grupo;

    -   Diretor Técnico: até 03 integrantes do grupo;

    -   Desenvolvedor: sem limites de alunos.

## Repositório do projeto

Todos os projetos deverão estar armazenados em repositório (GitHub,
GitLab, etc.), em todas as fases do ciclo de desenvolvimento. Alguns
requisitos básicos:

-   Realizar boa documentação dos arquivos e incluir controle de
    alterações e versões, à medida que o projeto avança;

-   Cada área técnica deverá possuir uma pasta especifica, contendo
    todos os arquivos utilizados no projeto;

-   Incluir uma pasta com relatórios e apresentações dos pontos de
    controle.

## Avaliações 

A avaliação dos alunos será feita de forma contínua a partir de três
pontos de controle, C1 a C3. Para cada avaliação será atribuída uma nota
entre 0 (zero) e 10 (dez) pontos, sendo que a menção final (*N_f*) será
dada pela seguinte fórmula:

<math xmlns="http://www.w3.org/1998/Math/MathML">
  <msub>
    <mrow>
      <mi mathvariant="bold-italic" mathcolor="#000000">N</mi>
    </mrow>
    <mrow>
      <mi mathvariant="bold-italic" mathcolor="#000000">f</mi>
    </mrow>
  </msub>
  <mo mathcolor="#000000">=</mo>
  <mfrac mathcolor="#000000">
    <mrow>
      <mi mathvariant="bold-italic" mathcolor="#000000">C</mi>
      <mn mathcolor="#000000">1</mn>
      <mo mathcolor="#000000">+</mo>
      <mn mathcolor="#000000">4</mn>
      <mi mathvariant="bold-italic" mathcolor="#000000">C</mi>
      <mn mathcolor="#000000">2</mn>
      <mo mathcolor="#000000">+</mo>
      <mn mathcolor="#000000">4</mn>
      <mi mathvariant="bold-italic" mathcolor="#000000">C</mi>
      <mn mathcolor="#000000">3</mn>
    </mrow>
    <mrow>
      <mn mathcolor="#000000">9</mn>
    </mrow>
  </mfrac>
</math>


-   **Para C1:** Entrega de relatório e apresentação dos resultados das
    fases 1, 2 e 3 do ciclo de vida do projeto. Além dos objetivos
    previamente definidos, serão observados os seguintes pontos:

    -   Entendimento do problema;

    -   Concepção da arquitetura básica da solução;

    -   Organização gerencial do projeto.

        -   Alocação de atividades para cada um dos membros da equipe

    -   **Pré-projeto da solução, contendo os seguintes itens de
        documentação:**

        -   Desenhos mecânicos refinados, com as indicações do tipo de
            material, cotas, dentre outras informações que auxiliem na
            fabricação das estruturas mecânicas do sistema. Estes
            desenhos devem ser o esquemático de 3D, bem como os cortes
            dos equipamentos;

        -   Diagramas elétricos e eletrônicos do sistema, sendo composto
            por diagramas unifilares/trifilares (com os dispositivos de
            proteção, seccionamento, seção de fios, etc.) de sistemas de
            alimentação, diagramas esquemáticos de circuitos eletrônicos
            (com identificação dos componentes eletrônicos que serão
            utilizados nos circuitos), diagramas detalhando barramentos
            de alimentação dos circuitos eletrônicos (ou seja, trata-se
            da interface entre sistemas de alimentação e circuitos
            eletrônicos), diagramas com detalhes de lógicas e protocolos
            de comunicação entre elementos ( microcontrolador com 
            microcontrolador, microcontrolador e sensor, 
            microcontrolador e atuador,  microcontrolador e software,
            etc);

        -   Em relação aos softwares, espera-se aspectos de documentação
            de software: diagramas de classes (se aplicado), diagramas
            de casos de uso, diagramas com protocolos de comunicação
            entre componentes do software, etc.

-   **Para C2:** Entrega de relatório e apresentação dos resultados da
    fase 3 do ciclo de vida do projeto, de acordo com os objetivos
    previamente definidos. Além dos objetivos previamente definidos,
    serão observados os seguintes pontos:

    -   Neste ponto de controle, os grupos deverão apresentar adequações
        realizadas no projeto, que tenham impactado a especificação de
        componentes, protocolos, algoritmos dos subsistemas que compõe a
        solução;

    -   **Neste ponto de controle, os grupos deverão realizar uma
        demonstração de funcionamento de todos os subsistemas que compõe
        a solução, já considerando a integração entre as respectivas
        partes;**

-   **Para C3:** Apresentação dos resultados da fase 4 do ciclo de vida
    do projeto, de acordo com os objetivos previamente definidos. Além
    dos objetivos previamente definidos, serão observados os seguintes
    pontos:

    -   Testes de integração e funcionamento do protótipo de produto;

    -   **Demonstração do funcionamento completo da solução;**

    -   Documentação técnica atualizada nos repositórios do projeto;

    -   Bannner de apresentação do projeto;

    -   Vídeo de propaganda da solução, contemplando seu funcionamento
        completo


**Observações:**

-   Para a aprovação, é necessário que a frequência do aluno às aulas
    seja maior ou igual a 75% e que *N~f~* seja maior ou igual a 5,0;

-   Em todos os pontos de controle, todos os integrantes de um grupo
    devem realizar uma autoavaliação e anexá-la ao relatório entregue
    pelo grupo. Esta autoavaliação consistirá em uma tabela com os nomes
    dos alunos do grupo e uma descrição de como cada um deles contribuiu
    individualmente para o projeto.

-   No caso do projeto, como um todo, não funcionar, a situação de cada
    subsistema poderá ser considerada em separado. Se um dado subsistema
    tiver funcionado, mesmo que não integrado devido a falhas de outros
    subsistemas, os subgrupos de alunos responsável por este subsistema
    precisará demonstrar que o seu subsistema funcionou. Seguem-se
    exemplos:

    -   Os alunos de engenharia de software deverão desenvolver um
        emulador (que inclusive servirá como artefato de teste) que faça
        as vezes da interface com o resto do produto e, com este,
        mostrar que o software está funcionando corretamente.

    -   Os alunos de engenharia de eletrônica deverão mostrar que, dados
        certos valores de sinais de entrada nas placas/circuitos
        eletrônicos, os valores dos sinais de saída correspondem ao
        projetado/esperado.

    -   E da mesma forma os alunos de engenharia de energia, engenharia
        automotiva e engenharia aeroespacial deverão comprovar a
        operacionalidade de seu subsistema, por meio de uma rotina de
        testes que comprove que os seus desenvolvimentos funcionariam,
        caso estivessem integrados às outras partes do projeto.

-   Os pontos de controle se caracterizam por avaliações das
    áreas/subsistemas do produto proposto, levando em consideração os
    respectivos avanços e contribuições para o desenvolvimento do
    produto final;

-   As notas do ponto de controle serão aplicadas pelas áreas e/ou
    subsistemas do projeto e, nesse sentido, a nota do ponto de controle
    não será única para todo o grupo. Além do mais, para a definição da
    nota individual do ponto de controle, será levado em consideração as
    atividades desenvolvidas por cada integrante, bem como os resultados
    alcançados, tendo em vista seu papel do grupo;

-   Tendo em vista o caráter multidisciplinar desta disciplina, as notas
    finais dos pontos de controle advêm da avaliação **por uma banca
    examinadora composta por professores** dos cursos de engenharia da
    FGA: aeroespacial, automotiva, eletrônica, energia e software.
    Assim, o resultado final de cada ponto de controle será obtido a
    partir da média da avaliação individual de cada professor membro da
    banca;

-   Após os pontos de controle, a banca examinadora apresentará um
    feedback acerca do desempenho de cada subsistema, tendo em vista as
    atividades planejadas e executadas no projeto;

-   A critério dos professores-tutores, entre os pontos de controle
    poderão ser realizadas reuniões de acompanhamento. Estas reuniões
    não terão cunho avaliativo e se destinarão, exclusivamente, ao
    acompanhamento das atividades;

-   Os estudantes serão arguidos pela banca examinadora durante os
    pontos de controle;

-   As solicitações de revisão de menção deverão ser realizadas via o
    processo formal na secretaria da FGA.

## Calendário de atividades da disciplina

| **Atividade**                       | **Data**                    |
|-------------------------------------|-----------------------------|
| Apresentação do plano de ensino     | 20/03/2024                  |
| Definição de temas e grupos         | 22/03/2024 a 27/03/2024     |
| Entrega de relatório do Ponto de Controle 1 e links dos repositórios de projeto | 5 dias antes da apresentação |
| Ponto de Controle 1 (PC1)           | 03/05/2024, 08/05/2024 e 10/05/2024 |
| Entrega de relatório do Ponto de Controle 2 | 5 dias antes da apresentação |
| Ponto de Controle 2 (PC2)           | 05/06/2024, 07/06/2024 e 12/06/2024 |
| Ponto de Controle 3 (PC3)           | 26/06/2024, 28/06/2024 e 03/07/2024 |
| Reapresentação do PC3*              | 10/07/2024                  |
| Apresentação de projetos na FIT/FGA | 10/07/2024                  |

\*Datas podem ser ajustadas em função da quantidade de grupos

**Observações:**

-   Os relatórios deverão ser enviados por email para os professores;

-   Para os pontos de controle, a ordem e tempo de apresentação serão
    definidos pelos professores e divulgados com a devida antecedência;

-   Todos os integrantes do grupo devem estar presentes nos pontos de
    controle. Ausências não justificadas poderão produzir sanções para o
    estudante e grupo;

-   Data de reapresentação do PC3: apenas para grupos que apresentaram a
    solução integrada e não fizeram todos os testes de funcionamento.
    Tais grupos serão definidos pela banca de professores, baseado nos
    critérios estabelecidos para a fase 4 da elaboração dos projetos e
    em função dos resultados apresentados no PC3. Se um grupo não
    atender tais critérios, ele não terá direito à reapresentação do
    trabalho,

## Referências Bibliográficas

### Bibliografia básica:

\[1\] PROJECT MANAGEMENT INSTITUTE -- PMI. Guide of Project Management
Body of Knowledge - PMBOK, 2013.

\[2\] PAHL, G. Projeto na engenharia: fundamentos do desenvolvimento
eficaz de produtos, métodos e aplicações. São Paulo: Edgard Blücher,
2011. xvi, Quantidade : 10 412 p. ISBN 9788521203636.

[Bibliografia Complementar: ]{.underline}

\[1\] Pahl, G., Beitz, W., Engineering Design -- A Systematic Approach,
Springer-Verlag, 1996.

\[2\] Baxter, M., Projeto de Produto -- Guia prático para o design de
novos produtos, 2da ed. Edgar Blucher, 1998.

\[3\] Valeraino, D., Gerência em Projetos: Pesquisa, Desenvolvimento e
Engenharia, Makron, 2004. 


## Apêndice 01 – Detalhamento de atividades

O detalhamento das atividades referentes a cada papel está descrito na sequência.

### Coordenador Geral:

O Coordenador Geral tem por objetivo assegurar a condução operacional dos trabalhos e desenvolvimentos, envolvendo o planejamento e gestão de atividades, projeção de resultados e antecipação de riscos. O Coordenador Geral tem por responsabilidade básica a boa condução do projeto e das equipes técnicas, possuindo uma visão geral de todas as atividades que estão em desenvolvimento, e tendo forte interação com o Diretor de Qualidade e Diretores Técnicos. Embora tais fatos, o Coordenador Geral deverá contribuir na concepção e desenvolvimento de atividades técnicas.

#### Atividades a serem desenvolvidas:

- Realizar o planejamento de atividades das equipes técnicas;
- Realizar a gestão de tempo e riscos do projeto, bem como realizar o controle financeiro do projeto;
- Atuar na definição e validação de requisitos técnicos, de forma a garantir que a arquitetura da solução atenda às necessidades do cliente;
- Assegurar todos os “entregáveis”;
- Validar o plano de produção e integração dos produtos das equipes técnicas.

### Diretor de Qualidade:

O Diretor de Qualidade tem por objetivo assegurar a condução operacional dos trabalhos e desenvolvimentos, dando suporte às atividades do coordenador geral e garantindo que os produtos atendam aos requisitos técnicos do projeto. Embora tais fatos, o Diretor de Qualidade deverá contribuir na concepção e desenvolvimento de atividades técnicas.

#### Atividades a serem desenvolvidas:

- Suporte ao Coordenador Geral na tarefa de planejamento e gestão das atividades dos grupos técnicos;
- Atuar na definição de requisitos técnicos e na tomada de decisão sobre a arquitetura da solução;
- Garantir que os produtos desenvolvidos no projeto atendam aos requisitos técnicos;
- Garantir que os critérios de projeto adotados pelas equipes técnicas estejam em consonância com boas práticas de engenharia. Ou seja, validar as justificativas de escolhas técnicas e tecnológicas pelas equipes;
- Gerenciar a garantir a integração entre os produtos dos subsistemas;
- Desenvolvimento e validação de documentação técnica do projeto.

### Diretor Técnico:

O Diretor Técnico tem por objetivo garantir a condução operacional de sua equipe, a qual se constitui em um subsistema do projeto. Suas atividades envolvem o planejamento e gestão de atividades técnicas, bem como a interação com o Coordenador Geral e o Diretor de Qualidade para o desenvolvimento do projeto segundo os planejamentos.

#### Atividades a serem desenvolvidas:

- Gerenciar as atividades dos desenvolvedores e garantir a coesão do grupo;
- Atuar na definição de requisitos técnicos e tomada de decisão sobre tecnologias aplicáveis ao projeto e ao subsistema;
- Atuar na definição e aplicação de critérios de projeto, para garantir a correta especificação dos elementos;
- Atuar na validação de produtos dos desenvolvedores e garantir a interoperabilidade dos produtos entre subsistemas;
- Atuar na definição de planos de produção e integração entre produtos de diferentes equipes;
- Desenvolver e validar a documentação técnica da equipe;

### Desenvolvedor:

O Desenvolvedor tem por objetivo aplicar os conhecimentos técnicos para a produção e elaboração dos elementos que compõem o projeto. Nesse sentido, o desenvolvedor atua na implementação das soluções concebidas pela equipe, tendo em vista os requisitos técnicos associados ao problema e as tecnologias escolhidas.

#### Atividades a serem desenvolvidas:

- Identificar os requisitos técnicos e definição de tecnologias para resolver o problema abordado no projeto. A escolha das tecnologias deve ser justificada pelos desenvolvedores;
- Desenvolver e validar as partes técnicas sob sua responsabilidade;
- Definir critérios de produção e interoperabilidade dos produtos do seu subsistema;
- Desenvolver as documentações técnicas, referentes aos itens sob seu desenvolvimento;
- Participar da integração dos elementos de seu subsistema ao restante do projeto.


